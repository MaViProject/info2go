/*
** This file contains the code that performs the webservice calls and the structures to store the returns.
 */
package main

import (
	"encoding/json"
	"errors"
)

type RandomFact struct {
	Fact string `json:"text"`
}

type RandomChuck struct {
	Chuck string `json:"value"`
}

type Cocktail struct {
	Drink        string `json:"strDrink"`
	Category     string `json:"strAlcoholic"`
	Glass        string `json:"strGlass"`
	Image        string `json:"strDrinkThumb"`
	Ingredient1  string `json:"strIngredient1"`
	Ingredient2  string `json:"strIngredient2"`
	Ingredient3  string `json:"strIngredient3"`
	Ingredient4  string `json:"strIngredient4"`
	Ingredient5  string `json:"strIngredient5"`
	Ingredient6  string `json:"strIngredient6"`
	Ingredient7  string `json:"strIngredient7"`
	Ingredient8  string `json:"strIngredient8"`
	Ingredient9  string `json:"strIngredient9"`
	Ingredient10 string `json:"strIngredient10"`
	Measure1     string `json:"strMeasure1"`
	Measure2     string `json:"strMeasure2"`
	Measure3     string `json:"strMeasure3"`
	Measure4     string `json:"strMeasure4"`
	Measure5     string `json:"strMeasure5"`
	Measure6     string `json:"strMeasure6"`
	Measure7     string `json:"strMeasure7"`
	Measure8     string `json:"strMeasure8"`
	Measure9     string `json:"strMeasure9"`
	Measure10    string `json:"strMeasure10"`
	ReceiptEN    string `json:"strInstructions"`
	ReceiptIT    string `json:"strInstructionsIT"`
}

type Cocktails struct {
	CocktailList []Cocktail `json:"drinks"`
}

// formatted drink as the structure returned is horrible
type Drink struct {
	Name        string
	Category    string
	Glass       string
	Image       string
	Ingredients []string
	Receipt     string
}

// Directly calls the random fact (to make it parametric)
func getRandomFact() (fact string, e error) {
	var rfact RandomFact
	resp, e := client.Get("https://uselessfacts.jsph.pl/random.json?language=en")
	if e != nil {
		return fact, e
	}
	defer resp.Body.Close()
	e = json.NewDecoder(resp.Body).Decode(&rfact)
	if e != nil {
		return fact, e
	}
	fact = rfact.Fact + " - by UselessFacts"
	return fact, nil
}

// Directly calls the random Chuck Norris facts
func getRandomChuck() (chuck string, e error) {
	var rchuck RandomChuck
	resp, e := client.Get("https://api.chucknorris.io/jokes/random")
	if e != nil {
		return chuck, e
	}
	defer resp.Body.Close()
	e = json.NewDecoder(resp.Body).Decode(&rchuck)
	if e != nil {
		return chuck, e
	}
	chuck = rchuck.Chuck + " - by ChuckFacts"
	return chuck, nil
}

// may return a cocktail list but only 1 is returned by design
func getCocktail(search, param string) (drink Drink, e error) {
	var cocktails Cocktails
	var callString string
	if search == "" { //random cocktail
		callString = "https://www.thecocktaildb.com/api/json/v1/1/random.php"
	} else {
		if param == "" {
			e = errors.New("Searching for " + search + " but no indication for name or ingredient.")
			return drink, e
		} else if param == "base" { //searching for cocktail base ingredient
			callString = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=" + search
		} else if param == "name" { //searching for the cocktail name
			callString = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=" + search
		} else { //param is not recognized
			e = errors.New(param + " is not an optionable search param for " + search + ".")
			return drink, e
		}
	}
	resp, e := client.Get(callString)
	if e != nil {
		return drink, e
	}
	defer resp.Body.Close()
	e = json.NewDecoder(resp.Body).Decode(&cocktails)
	if e != nil {
		return drink, e
	}
	// put the single cocktail in the drink
	drink.Name = cocktails.CocktailList[0].Drink
	drink.Category = cocktails.CocktailList[0].Category
	drink.Glass = cocktails.CocktailList[0].Glass
	drink.Image = cocktails.CocktailList[0].Image
	drink.Receipt = cocktails.CocktailList[0].ReceiptEN
	if cocktails.CocktailList[0].Ingredient1 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient1+": "+cocktails.CocktailList[0].Measure1)
	}
	if cocktails.CocktailList[0].Ingredient2 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient2+": "+cocktails.CocktailList[0].Measure2)
	}
	if cocktails.CocktailList[0].Ingredient3 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient3+": "+cocktails.CocktailList[0].Measure3)
	}
	if cocktails.CocktailList[0].Ingredient4 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient4+": "+cocktails.CocktailList[0].Measure4)
	}
	if cocktails.CocktailList[0].Ingredient5 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient5+": "+cocktails.CocktailList[0].Measure5)
	}
	if cocktails.CocktailList[0].Ingredient6 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient6+": "+cocktails.CocktailList[0].Measure6)
	}
	if cocktails.CocktailList[0].Ingredient7 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient7+": "+cocktails.CocktailList[0].Measure7)
	}
	if cocktails.CocktailList[0].Ingredient8 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient8+": "+cocktails.CocktailList[0].Measure8)
	}
	if cocktails.CocktailList[0].Ingredient9 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient9+": "+cocktails.CocktailList[0].Measure9)
	}
	if cocktails.CocktailList[0].Ingredient10 != "" {
		drink.Ingredients = append(drink.Ingredients, cocktails.CocktailList[0].Ingredient10+": "+cocktails.CocktailList[0].Measure10)
	}

	return drink, nil
}
