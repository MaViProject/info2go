package main

import (
	"bufio"
	"fmt"
	"image/color"
	"math/rand"
	"net/http"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

// Structures

type WSize struct {
	w, h float32
}

// Global variables
var (
	// info var
	__app__     string = "Info2Go"
	__version__ string = "1.0.0"
	__author__  string = "Mario Vitale"

	client *http.Client //client to use to perform the GET against the services
)

// Init the application checking config file is on
func init() {
	// welcome message
	welcome := __app__ + " by " + __author__ + " - Running version: " + __version__
	fmt.Println(welcome)
}

func main() {
	//set the params
	//rgba(67, 255, 100, 0.85)
	textColor := color.RGBA{R: 3, G: 160, B: 90, A: 0}
	wsize := WSize{w: 800, h: 300}
	var bigFont float32 = 24

	client = &http.Client{Timeout: 10 * time.Second}

	//fyne window (theme, title and size)
	a := app.New()
	a.Settings().SetTheme(DarkMintTheme{})
	win := a.NewWindow("Info 2 Go")
	win.Resize(fyne.NewSize(wsize.w, wsize.h))

	/*** FACTS ***/

	// Fact title
	ftitle := canvas.NewText("Get Your Useless Facts", textColor)
	ftitle.TextStyle = fyne.TextStyle{
		Bold: true,
	}
	ftitle.Alignment = fyne.TextAlignCenter
	ftitle.TextSize = bigFont

	//widget store the fact text retrieved by button, wraps on a new line while resizing
	var fact string
	var err error
	factText := widget.NewLabel("")
	factText.Wrapping = fyne.TextWrapWord
	// Button to generate a fact
	fbutton := widget.NewButton("Get Fact", func() {
		//decide randomly for a chuck fact or a useless fact
		rand.Seed(time.Now().UnixNano())
		randomVal := rand.Intn(10)
		if randomVal <= 5 {
			fact, err = getRandomFact()
		} else {
			fact, err = getRandomChuck()
		}
		if err != nil {
			dialog.ShowError(err, win)
		} else {
			i := 0
			cursor := false
			ticker := time.NewTicker(time.Millisecond * 50)
			go func() {
				for range ticker.C {
					if i < len(fact) {
						if cursor {
							factText.SetText(fact[:i] + "_")
						} else {
							factText.SetText(fact[:i])
						}
						cursor = !cursor
						i++
					} else {
						ticker.Stop()
					}
				}
			}()
			//factText.SetText(fact)
		}
	})
	//Facts Tab content
	fhBox := container.New(layout.NewHBoxLayout(), layout.NewSpacer(), fbutton, layout.NewSpacer())
	fvBox := container.New(layout.NewVBoxLayout(), ftitle, fhBox, widget.NewSeparator(), factText)
	//Facts tab
	factsTab := container.NewTabItem("Facts", fvBox)

	/*** DRINKS ***/

	//Drink title
	dtitle := canvas.NewText("What do you want to drink today?", textColor)
	dtitle.TextStyle = fyne.TextStyle{
		Bold: true,
	}
	dtitle.Alignment = fyne.TextAlignCenter
	dtitle.TextSize = bigFont

	//Drink contents
	//Cocktail name
	drinkName := widget.NewLabel("")
	drinkName.Wrapping = fyne.TextWrapWord
	drinkName.TextStyle = fyne.TextStyle{
		Bold: true,
	}
	//Ingredient list
	drinkIngredients := widget.NewLabel("")
	drinkIngredients.Wrapping = fyne.TextWrapWord
	drinkIngredients.TextStyle = fyne.TextStyle{
		Italic: true,
	}
	//Receipt text
	drinkReceipt := widget.NewLabel("")
	drinkReceipt.Wrapping = fyne.TextWrapWord
	//Image
	var drinkImage canvas.Image

	// Button to generate a drink and its content
	dbutton := widget.NewButton("Get Cocktail", func() {
		drink, err := getCocktail("", "")
		if err != nil {
			dialog.ShowError(err, win)
		} else {
			drinkName.SetText(drink.Name)
			drinkReceipt.SetText(drink.Receipt)
			var ingrList string
			for _, ingr := range drink.Ingredients {
				ingrList = ingrList + "- " + ingr + "\n"
			}
			drinkIngredients.SetText(ingrList)
			//download the image and use it
			resp, err := http.Get(drink.Image)
			if err != nil {
				dialog.ShowError(err, win)
			}
			defer resp.Body.Close()

			//get the image from the downloaded stream
			drinkImage = *canvas.NewImageFromReader(bufio.NewReader(resp.Body), "cocktail")
			//set the minimum image size
			drinkImage.SetMinSize(fyne.NewSize(320, 240))
			drinkImage.Resize(fyne.NewSize(320, 240))
			//draw the image to fill the container
			drinkImage.FillMode = canvas.ImageFillStretch
			drinkImage.Refresh()
		}
	})
	//2 contents to store image and another 3 containers with name, ingredients, receipt
	//vertical container with Title, Ingredients and receipts
	ddatavBox := container.New(layout.NewVBoxLayout(), drinkName, drinkIngredients, drinkReceipt)
	ddataScroll := container.NewScroll(ddatavBox)
	ddataScroll.SetMinSize(fyne.NewSize(320, 240))
	//horizontal to put side by side image and cocktail info
	ddatahBox := container.New(layout.NewHBoxLayout(), &drinkImage)
	//Drinks Tab content
	dhBox := container.New(layout.NewHBoxLayout(), layout.NewSpacer(), dbutton, layout.NewSpacer())
	dvBox := container.New(layout.NewVBoxLayout(), dtitle, dhBox, widget.NewSeparator(), ddatahBox, ddataScroll)

	//Drinks tab
	drinksTab := container.NewTabItem("Drinks", dvBox)

	//Tab container to store the previously created tab
	tabs := container.NewAppTabs(factsTab, drinksTab)
	//tabs.Append(container.NewTabItemWithIcon("Home", theme.HomeIcon(), widget.NewLabel("Home tab")))
	tabs.SetTabLocation(container.TabLocationLeading)

	//Set the tabs container in the main window content and run
	win.SetContent(tabs)
	win.ShowAndRun()

}
